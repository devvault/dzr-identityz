modded class CorpseData
{
	PlayerIdentity m_PlayerIdentity;
	string 		m_CorpseName;
	
	void CorpseData(notnull PlayerBase player,int time_of_death)
	{
		m_PlayerIdentity = m_Player.GetIdentity();
		m_CorpseName = m_PlayerIdentity.GetName();
		
		////Print("[CorpseData] :: m_CorpseName: "+m_CorpseName);
		
		auto newIdentityZ = new Param2<PlayerBase, string>(player, m_CorpseName);
		GetGame().RPCSingleParam(player, DZRIDZRPC.SAVEPLAYERNAME, newIdentityZ, true);
		////Print("[DZR IdentityZ] ::: CorpseData ::: Sending to server m_CorpseName: "+newIdentityZ.param2);
		
	}
	
	override void UpdateCorpseState(bool force_check = false)
	{
		if (GetGame().IsServer() && GetGame().IsMultiplayer())
		{
			
			////Print("[DZR IdentityZ] ::: PlayerBase.c: UpdateCorpseState ::: IsServer: "+m_Player+", "+m_CorpseName);
			m_Player.SavePlayerName(m_CorpseName);
			super.UpdateCorpseState(force_check);
		}
		else {
			////Print("[DZR IdentityZ] ::: PlayerBase.c: UpdateCorpseState ::: IsClient: "+m_Player+", "+m_CorpseName);
			m_Player.SavePlayerName(m_CorpseName);
			super.UpdateCorpseState(force_check);
		}
	}
}

modded class PlayerBase
{
    protected string DZRIDZ_PlayerName;
    protected bool m_ShowNameToAll;
    
	override void EEKilled( Object killer )
	{
		PlayerIdentity 	m_PlayerIdentity;
		string	 		m_CorpseName;
		
		PlayerBase the_player = PlayerBase.Cast(this);
		
		m_PlayerIdentity = the_player.GetIdentity();
		m_CorpseName = m_PlayerIdentity.GetName();
		auto newIdentityZ = new Param2<PlayerBase, string>(the_player, m_CorpseName);
		GetGame().RPCSingleParam(the_player, DZRIDZRPC.SERVERSENDSNAME, newIdentityZ, true);
		////Print("[DZR IdentityZ] ::: PlayerBase.c: Sending to all clients m_CorpseName: "+newIdentityZ.param2);
		//GetGame().Chat( "[DZR IdentityZ] PlayerBase.c: Sending to all clients m_CorpseName: "+newIdentityZ.param2, "colorImportant" );
		the_player.SavePlayerName(m_CorpseName);
		
		auto data = new Param1<string>(m_CorpseName);
        GetGame().RPCSingleParam(the_player, DZRIDZRPC.LOADPLAYERRNAME, data, true);
		
		super.EEKilled( killer );
	}
	
    override void OnRPC(PlayerIdentity sender, int rpc_type, ParamsReadContext ctx)
    {
        super.OnRPC(sender, rpc_type, ctx);
		
        switch (rpc_type)
        {
			case DZRIDZRPC.SERVERSENDSNAME:
            {
				//	auto newIdentityZ = new Param1<PlayerBase, PlayerIdentity>(player, identity);
				////Print("[DZR IdentityZ] ::: PlayerBase: OnRPC Server :: Switched to SERVERSENDSNAME");
				Param2<PlayerBase, string> newIdentityZ;
				if (GetGame().IsServer())
				{					
					if (!ctx.Read(newIdentityZ)) {
						////Print("[DZR IdentityZ] ::: PlayerBase: OnRPC Server :: Received new player name newIdentityZ.param2: "+newIdentityZ.param2);
					}
				} 
				else 
				{
					if (!ctx.Read(newIdentityZ)) {
						////Print("[DZR IdentityZ] ::: PlayerBase: OnRPC Client :: Received new player name newIdentityZ.param2: "+newIdentityZ.param2);
					}
				};
				return;
                break;
			}
		}
	}
	
    string LoadPlayerName()
    {
        return DZRIDZ_PlayerName;
	}
	
    void SavePlayerName(string name)
    {
        DZRIDZ_PlayerName = name;
	}
	
    bool isShowingNameToAll()
    {
		//Print("[DZR IdentityZ] ::: PlayerBase: someone asked for isShowingNameToAll: "+m_ShowNameToAll);
        return m_ShowNameToAll;
	}
	
    bool ToggleShowNameToAll()
    {
		//Print("[DZR IdentityZ] ::: PlayerBase: someone toggles ToggleShowNameToAll to "+!m_ShowNameToAll);
        m_ShowNameToAll = !m_ShowNameToAll;
		return m_ShowNameToAll;
	}	
}