modded class MissionServer
{
	ref dzr_idz_config_data_class m_defaultConfig;
	ref dzr_idz_config_data_class m_existingConfig;
	const static string dzr_Idz_ProfileFolder = "$profile:\\";
	const static string dzr_Idz_TagFolder = "DZR\\";
	const static string dzr_Idz_ModFolder = "IdentityZ\\";
    const static string dzr_Idz_ConfigFile = "dzr_idz_config.json";
	
    void MissionServer()
    {
		//TODO: Load config
		Print("[DZR IdentityZ][Server] ::: MissionServer ::: Initiated");
		GetRPCManager().AddRPC( "DZR_IDZ_RPC", "DZR_MissionServer_GetDataFromServer", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_IDZ_RPC", "DZR_MissionServer_ToggleShowToAll", this, SingleplayerExecutionType.Both );
		DZR_MissionServer_ReadConfigFile();
	}
	
    override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
    {
        super.InvokeOnConnect(player, identity);		
        player.SavePlayerName(identity.GetName());
        ////Print("[DZR IdentityZ] ::: MissionServer InvokeOnConnect ::: Player name stored to CharacterName: "+identity.GetName());
		//auto newIdentityZ = new Param2<PlayerBase, string>(player, identity.GetName());
		//GetGame().RPCSingleParam(player, DZRIDZRPC.SAVEPLAYERNAME, newIdentityZ, true);
		//Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect");
		ref Param1<dzr_idz_config_data_class> m_Data = new Param1<dzr_idz_config_data_class>(GetDayZGame().ReadServerConfig());
		//Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect ReadServerConfig "+m_Data);
		GetRPCManager().SendRPC( "DZR_IDZ_RPC_TOclient", "DZR_MissionGameplay_SaveConfigOnClient", m_Data, true, identity);
		//Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect DZR_MissionGameplay_SaveConfigOnClient"+m_Data);
		
		
	

		
		
	}
	
	void DZR_MissionServer_ToggleShowToAll(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
        ref Param2< PlayerBase, int> data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
            if (data.param1 != NULL)
            {

                bool result =  data.param1.ToggleShowNameToAll();
				//Print("[DZR IdentityZ] ::: ToggleShowNameToAll: " +result);
                ref Param1<bool> m_Data = new Param1<bool>(result);
                GetRPCManager().SendRPC( "DZR_IDZ_RPC_TOclient", "DZR_MissionGameplay_SetShowingNameForSelf", m_Data, true, sender);
				
			}
		}
	}
	
	void DZR_MissionServer_GetDataFromServer(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
		////Print("Someone called DZR_MissionServer_GetDataFromServer");
        ref Param1<PlayerBase> data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
            if (data.param1 != NULL)
            {
                //string playerName    = data.param1.GetIdentity().GetName();
                string playerName    = data.param1.LoadPlayerName();
                string senderName    = sender.GetName();
                bool m_ShowNameToAll    = data.param1.isShowingNameToAll();
				////Print("[DZR IdentityZ] ::: "+senderName+" recognized: " +playerName + ", btw, that players has m_ShowNameToAll set to "+m_ShowNameToAll);
                //string Steam64ID     = data.param1.GetIdentity().GetPlainId();
                //string GUID          = data.param1.GetIdentity().GetId();
                //float currentHealth  = data.param1.GetHealth("GlobalHealth", "");
                //float currentBlood   = data.param1.GetHealth("GlobalHealth", "Blood");
                //ref Param6<string,string,string,float,float,int> m_Data = new Param6<string,string,string,float,float,int>(playerName,Steam64ID,GUID,currentHealth,currentBlood,data.param2);
                ref Param2<string, bool> m_Data = new Param2<string, bool>(playerName, m_ShowNameToAll);
                GetRPCManager().SendRPC( "DZR_IDZ_RPC", "DZR_MissionGameplay_GetNameFromServer", m_Data, true, sender);
				
			}
		}
	}
	
	void DZR_MissionServer_ReadConfigFile()
	{
		//Print("[DZR IdentityZ] Server ::: ACTIVE ");
		//PROPER CONFIG
		/*
			const static string dzr_Idz_ProfileFolder = "$profile:\\";
			const static string dzr_Idz_TagFolder = "DZR\\";
			const static string dzr_Idz_ModFolder = "IdentityZ\\";
		*/
		//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile");
		if (!FileExist(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder)){
			//NO DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO DZR");
			MakeDirectory(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder);	
			MakeDirectory(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder);	
			dzr_writeConfigFile();			
		}
		else 
		{
			//YES DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile YES DZR");
			if (!FileExist(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder)){
				//NO IDZ
				//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO IDZ");
				MakeDirectory(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder);
				dzr_writeConfigFile();
			}
			else 
			{
				//YES IDZ
				//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile YES IDZ");
				dzr_writeConfigFile();
			}
			
		};
		
		
		
		//PROPER CONFIG
		
	}
	
	void dzr_writeConfigFile(){
		//FILE HANDLING
		
		string m_configFilePath = dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder+dzr_Idz_ConfigFile;
		//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile");
		if (!FileExist(m_configFilePath)) 
		{
			//Print("[DZR IdentityZ] ::: (!FileExist(dzr_Idz_ProfileFolder)) "+dzr_Idz_ProfileFolder);
			if (!m_defaultConfig) 
			{
				//Print("[DZR IdentityZ] ::: (!m_defaultConfig)"+m_defaultConfig);
				m_defaultConfig = new dzr_idz_config_data_class();
				GetDayZGame().SaveConfigOnServer(m_defaultConfig);
				JsonFileLoader<dzr_idz_config_data_class>.JsonSaveFile(m_configFilePath, m_defaultConfig);
			}
			//Print("[DZR IdentityZ] ::: +(!m_defaultConfig) "+m_defaultConfig);
			GetDayZGame().SaveConfigOnServer(m_defaultConfig);
			JsonFileLoader<dzr_idz_config_data_class>.JsonSaveFile(m_configFilePath, m_defaultConfig);
		} 
		else 
		{
			//Print("[DZR IdentityZ] ::: +FileExist(dzr_Idz_ProfileFolder) "+dzr_Idz_ProfileFolder);
			JsonFileLoader<dzr_idz_config_data_class>.JsonLoadFile(m_configFilePath, m_existingConfig);
			m_defaultConfig = new dzr_idz_config_data_class();
			if (m_defaultConfig.ConfigVersion != m_existingConfig.ConfigVersion)
			{
				int hour;
				int minute;
				int second;
				int year;
				int month;
				int day;
				GetHourMinuteSecond(hour, minute, second);
				GetYearMonthDay(year, month, day);
				string suffix = year.ToString()+"-"+month.ToString()+"-"+day.ToString()+"_"+hour.ToString()+"."+minute.ToString()+"."+second.ToString();
				JsonFileLoader<dzr_idz_config_data_class>.JsonSaveFile(m_configFilePath+"_BAK_"+suffix, m_existingConfig);
				GetDayZGame().SaveConfigOnServer(m_defaultConfig);
				JsonFileLoader<dzr_idz_config_data_class>.JsonSaveFile(m_configFilePath, m_defaultConfig);
			}
			else 
			{
				JsonFileLoader<dzr_idz_config_data_class>.JsonLoadFile(m_configFilePath, m_existingConfig);
			}
			GetDayZGame().SaveConfigOnServer(m_existingConfig);
			//JsonFileLoader<dzr_idz_config_data_class>.JsonSaveFile(m_configFilePath, m_existingConfig);
		}
		Print("[DZR IdentityZ] ServerSide ::: ACTIVE (Version: "+m_existingConfig.ModVersion+"."+m_existingConfig.ConfigVersion+" DEBUG: "+m_existingConfig.EnableDebug+")");
		Print("[DZR IdentityZ] ::: RecognitionMode: "+m_existingConfig.RecognitionMode );
		Print("[DZR IdentityZ] ::: ShowNameMode: "+m_existingConfig.ShowNameMode );
		Print("[DZR IdentityZ] ::: MaskingMode: "+m_existingConfig.MaskingMode );
		Print("[DZR IdentityZ] ::: Crosshair: "+m_existingConfig.Crosshair );
		Print("[DZR IdentityZ] ::: AllowManualShow: "+m_existingConfig.AllowManualShow );
		Print("[DZR IdentityZ] ::: InstantManualShow: "+m_existingConfig.InstantManualShow );
		Print("[DZR IdentityZ] ::: ShowAliveName: "+m_existingConfig.ShowAliveName );
		Print("[DZR IdentityZ] ::: ShowDeadName: "+m_existingConfig.ShowDeadName );
		
		//FILE HANDLING
	}
	
}



