[RecognitionMode]
-----------------
When the name of the player you're loking at is requested from the server.
0 ```RECOMMENDED``` - Always. Other modes are for very special gameplay cases.
1 - Toggle button (see ingame controls).
2 - Hold button.


[ShowNameMode]
-----------------
If you look at the player, is the name shown always or something can hide it?
0 ```RECOMMENDED``` - Always
1 - Mask hides name but you can force show it using the manual button (see ingame controls) if [AllowManualShow] (below) is enabled.
2 - Name is hidden but holding the ID item in hands reveals it (requires [IdClassnames]).


[MaskingMode]
-----------------
If the mask hides the name, which masks hide it and how.
0 - Ignore masks. All masks are disabled and have no effect on showing names or not.
1 - Any mask hides your name (except glasses, but you can put glasses items to the full mask list below to make them hide the name).
2 ```RECOMMENDED``` - Enable half-masks. Very immersive mode. Depending on what item you wear on your face, the name is revealed from different distances. Items partially covering your face will make it a bit easier to recognize you. So glasses make the recognition distance a bit shorter. If you add any lower part mask like nios mask, surgical mask, bandana etc. results in full face coverage and is considered a full mask and the name is fully hidden. Wearing only the lower face part mask will hide the name more effective than glasses only. If you wear a transparent moto helmet, it is considered a half mask. Fully dimmed moto helmet is a full mask. On the other hand, players sometimes wear gas masks for protection, so all gas masks are considered half masks to balance out situations when, for example, you must know a person's name, but removing gas mask will harm the player. So you just come closer and see the name without needing to remove gas mask.


[AllowManualShow]
-----------------
The Superfirendly mode. Set to true ``RECOMMENDED`` to allow using the key to manually override any masks or other conditions that hide your name. So you can wear any masks, be at max distance, at night but with this mode enabled everyone will see your name clearly. When active you will see the green mask icon. Useful if you want to wear a mask (for being warm or whatever), but don't want to hide your name. Taking off a mask requires messing with the inventory, but this shortcut helps doing it instantly.


[InstantManualShow]
-----------------
Set to true ```RECOMMENDED``` to ignore any delay settings below. If the Superfriendly mode is ON, your name will appear instantly to others.


[ShowAliveName]
-----------------
Set to false to hide names for any alive players. Ignores all the settings above.


[ShowDeadName]
-----------------
Set to true ```RECOMMENDED``` to allow players recognize names on dead bodies.


[Crosshair (dot)]
-----------------
0 - OFF ```RECOMMENDED```
1 - Always ON
2 - ON when in recognition mode.


[MaskingIndication]
-----------------
0 - OFF
1 - Always ON ```RECOMMENDED```
2 - ON when in recognition mode.


[Indicators show how your face is masked:]
-----------------
- Full white mask — your face is fully covered.
- Half-cut transparent mask — you are wearing either glasses or a mask covering only half of your face.
- Green mask — you activated AllowManualShow mode and anyone will see your name\face even if you wear a mask.
- Green Mask with ID — your ID is in hand or in the badge slot and it activates unrestricted name showing.


[DelayShow_ms] ``(ms = milliseconds)``
-----------------
Time before the name is shown when you look at a player.


[DelayHide_ms]
-----------------
Time before the name is hidden when you look away from the player


[NameUpdateFrequency_ms]
-----------------
How often you request target name from server 
[!!!WARNING!!!] Very small NameUpdateFrequency_ms value may reduce server and client performance, freeze game or even crash the server.


[VisibleDistance_m] ``(m = meters)``
-----------------
The name is never shown beyond that distance.


[DeadDistanceModifier_m]
-----------------
Subtract from VisibleDistance_m if the target is dead.


[NightModifier_m]
-----------------
Subtract from VisibleDistance_m if the Sun is below the horizon.


[OvercastModifier_m]
-----------------
(Only at night) Multiplied by cloudiness intensity and is subtracted from VisibleDistance_m.


[FogModifier_m]
-----------------
(Only at night) Multiplied by fogginess intensity and is subtracted from VisibleDistance_m.


[NightMultiplier]
-----------------
Multiplies FogModifier_m + OvercastModifier_m. Allows more precise night recognition tweaking with a single coefficient.


[Settings for MaskingMode = 2]
-----------------
[minVisibleDistance_m]
Used to allow everyone to see names within that distance.

[FullMaskHide_m]
Subtract that from VisibleDistance_m if wearing a full mask.

[HalfMaskHide_m]
Subtract that from VisibleDistance_m if wearing a half mask.

[HalfEyeMaskHide_m]
Subtract that from VisibleDistance_m if wearing glasses. 

[GasMaskHide_m]
Subtract that from VisibleDistance_m if wearing a gasmask.


[IdClassnames]
-----------------
List of items to act as the ID in ShowNameMode = 2. Try "Paper" or use some passport mods.


[List of masks by types]
-----------------
[FullMaskClassnames] — masks fully hiding the name.
[HalfMaskClassnames] — half masks for MaskingMode 2.
[HalfEyeMaskClassnames] — glasses for MaskingMode 2.
[GasMaskClassnames] — gas masks that would act as half masks in MaskingMode 2.
[ExcludedHeadgear] — Special list of items that should not be acting as masks when worn as headgear. There are certain hybrid items that transform into different items in different slots. So the bandanas can be put on the head and on the face to cover the lower part. So you should include such miltislot items here otherwise, if worn on the head, they will be acting like a mask.


[EnableDebug]
-----------------
Display diagnostic information to see how masking and distances change. Allows seeing your own name when you fly with the admin camera. In this mode the distance is calculated not from the character's head, but from the camera. It will help checking distances without moving a character.


[Changelog]
-----------------
Nevermind :) I sometimes write some notes to admins here.


[ModVersion]
-----------------
``Version number explanation``
M.m.R.C as in 1.0.5.44
M = Major version, like complete overhaul or engine change.
m = Minor version, important or multiple feature addition
R = Revision, updated after some fixes or tweaks applied.
C = Config structure version, see below.


[ConfigVersion]
-----------------
If the mod config is updated and your version is different, that means your old config is no longer compatible with the current version of the mod. Your old config will be backed up and the new config will be applied with the default settings (basic mode, see below). You can restore your settings from your backup file manually.


[Useful Config Snippets]

Basic mode with all masking features. You just look at the player and see name. Masks hide the name. Wearing glasses or half masks requires coming closer to see the name.
    "RecognitionMode": 0,
    "ShowNameMode": 1,
    "MaskingMode": 2,

Hard RP mode. Name only shown when you hold the ID item.
    "RecognitionMode": 0,
    "ShowNameMode": 2,
    "MaskingMode": 0,
	
Easy logic mode. You always see names. Any mask hides it.
    "RecognitionMode": 0,
    "ShowNameMode": 0,
    "MaskingMode": 1,

I'm always glad to help you with configuring the mod. I promise, we will find a perfect configuration for any gameplay style on your server. I will help with classnames etc. I also love to hear suggestions for this or any other of my mods, or even a fresh mod idea. Find me on Discord Mikhail#0937 or leave a message in Steam, at the mod comments or in private messages.


[KNOWN BUGS]
-----------------
- When you first start a server with this mod and you don't have a config. It will be created for you, but sometimes the server sees no config and can crash or ignore the mod. Just restart it.
- Sometimes for the same cause as above the new version of the config is not applied until you restart the server.
- Light sources do not improve recognition at night.
- The name is blinking a couple of times before disappearing.

